const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const {VueLoaderPlugin} = require('vue-loader');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname,'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        // js: 'babel-loader',
                        scss: 'vue-style-loader!css-loader!sass-loader'
                    }
                }
            }
        ]
    },
    resolve:{
        alias : {
            'vue$':'vue/dist/vue.js'
        }
    },
    plugins: [
        new ExtractTextPlugin('style.css'),
        new VueLoaderPlugin()
    ]
};
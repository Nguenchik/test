import axios from "axios";

export default {
    actions:{
        async takePosts(context){
            const response = await axios('https://jsonplaceholder.typicode.com/posts?_limit=3');
            const posts = await response.data;
            context.commit('updatePosts', posts)
        }
    },
    mutations:{
        updatePosts(state, posts){
            state.posts = posts
        }
    },
    state:{
        posts:[]
    },
    getters:{
        getPosts(state) {
            return state.posts
        }
    }
}
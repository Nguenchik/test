import Vue from 'vue';
import Store from './store/store'

import ElementUI from 'element-ui';


Vue.use(ElementUI);

Vue.component('App', require('./components/App.vue').default);

const app = new Vue({
    el: '#app',
    store: Store
});
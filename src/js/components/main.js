import {mapGetters,mapActions} from "vuex";
import axios from "axios";

export default {
    methods:{
        toggle(row) {
            const {id, userId, title, body} = row;

            axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
                id: id,
                title: title,
                body: body,
                userId: userId
            })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });

            this.$refs.postsData.toggleRowExpansion(row);
        },
        ...mapActions(
            ['takePosts']
        ),

    },

    async mounted (){
        this.takePosts()
    },

    computed:{
        ...mapGetters(
            ['getPosts']
        ),
    }
}